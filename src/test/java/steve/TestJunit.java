package steve;

import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.fail;


class TestJunit {
    @Disabled
    @Test
    @DisplayName("Should demonstrate a simple assertion")
    void shouldShowSimpleAssertion(){
        Assertions.assertEquals(1, 2);
    }

    @Disabled
    @Test
    @DisplayName("Should check all items in the list")
    void shouldCheckAllItemsInTheList(){
        List<Integer> numbers=Arrays.asList(2, 3, 5, 7);

        Assertions.assertAll(()->Assertions.assertEquals(2, numbers.get(0)),
                ()->Assertions.assertEquals(3, numbers.get(1)),
                ()-> Assertions.assertEquals(5, numbers.get(2)),
                ()->Assertions.assertEquals(7, numbers.get(3)));
    }


}
