package steve.controllers;

import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import steve.models.CPU;
import steve.services.CpuService;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

class CpuControllerTest {
    @InjectMocks
    private CpuController cpuController=new CpuController();
    @Mock
    private CpuService cpuService;

    @BeforeEach
    void initMocks(){
        MockitoAnnotations.initMocks(this);
    }
    @Test
    void testHandleGetAllCpus(){
        List<CPU> cpuList=new ArrayList<>();
        cpuList.add(new CPU(101, "c1-cpu", 6, 3.0, 300));
        cpuList.add(new CPU(102, "c2-cpu", 6, 3.5, 350));

        Context ctx=mock(Context.class);
        when(cpuService.getAllCpus()).thenReturn(cpuList);
        cpuController.handleGetAllCpus(ctx);
        verify(ctx).json(cpuList);
    }
}