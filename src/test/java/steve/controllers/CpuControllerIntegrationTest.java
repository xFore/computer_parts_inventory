package steve.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.*;
import steve.JavalinApp;
import steve.models.CPU;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class CpuControllerIntegrationTest {
    private static JavalinApp app=new JavalinApp();

    @BeforeAll
    public static void startService(){
        app.start(80);
    }

    @BeforeAll
    public static void stopService(){
        app.stop();
    }

    @Disabled
    @Test
    public void testGetAllCpusUnauthorized(){
        HttpResponse<String> response = Unirest.get("http://stevetrainingblob.blob.core.windows.net/ecommerce-frontend/cputable.html").asString();
        assertAll(
                ()->assertEquals(401, response.getStatus()),
                ()->assertEquals("Unauthorized", response.getBody()));
    }

    @Disabled
    @Test
    public void testGetAllItemsAuthorized(){
        HttpResponse<List<CPU>> response = Unirest.get("http://stevetrainingblob.blob.core.windows.net/ecommerce-frontend/cputable.html")
                .header("Authorization", "admin-auth-token")
                .asObject(new GenericType<List<CPU>>() {});
        assertAll(
                ()->assertEquals(200,response.getStatus()),
                ()->assertTrue(response.getBody().size()>0)
        );
    }

}
