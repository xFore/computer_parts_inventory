package steve.controllers;

import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import steve.models.Customer;
import steve.services.CustomerService;

public class CustomerController {
    private final Logger logger = LoggerFactory.getLogger(CustomerController.class);
    private final CustomerService service = new CustomerService();

    public void handleGetAllCustomers(Context ctx) {
        logger.info("getting all Customers");
        ctx.json(service.getAllCustomers());
    }

    public void handleGetCustomerById(Context ctx) {
        String idString = ctx.pathParam("customerid");
        if (idString.matches("^\\d+$")) {
            int idInput = Integer.parseInt(idString);
            Customer customer = service.getCustomerById(idInput);
            if (customer == null) {
                logger.warn("No customer present with id: {}", idInput);
                throw new NotFoundResponse("No Customer found with provided ID: " + idInput);
            } else {
                logger.info("Getting Customer with ID: {}", idInput);
                ctx.json(customer);
            }
        } else {
            throw new BadRequestResponse("input \"" + idString + "\" cannot be parsed to an int");

        }
    }

    public void handleGetCustomerByUsername(Context ctx) {
        String username = ctx.pathParam("username");
        Customer customer = service.getCustomerByUsername(username);
        logger.info("Getting Customer with Username: {}", username);
        ctx.json(customer);
    }

    public void handlePostNewCustomer(Context ctx) {
        Customer customer = ctx.bodyAsClass(Customer.class);
        logger.info("Adding new Customers: {}", customer);
        service.addNewCustomer(customer);
        ctx.status(201);
    }

    public void handleDeleteCustomerById(Context ctx) {
        String idString = ctx.pathParam("customerid");
        if (idString.matches("^\\d+$")) {
            int idInput = Integer.parseInt(idString);
            logger.warn("Deleting Customer with ID: {}", idInput);
            service.deleteCustomer(idInput);
        } else {
            throw new BadRequestResponse("input \"" + idString + "\"cannot be parsed to an int");
        }
    }

    public void handleUpdateCustomerUsername(Context ctx){
        String username=ctx.queryParam("username");
        String idString=ctx.queryParam("customer_id");
        int customerId;
        if (idString != null) {
            customerId = Integer.parseInt(idString);
        }else{
            throw new BadRequestResponse("ID input cannot be null");
        }
        if(username!=null){
            logger.info("Updating Customer Username");
            service.updateCustomerUsername(username, customerId);
        }else{
            throw new BadRequestResponse("Username input cannot be null");
        }
    }
}
