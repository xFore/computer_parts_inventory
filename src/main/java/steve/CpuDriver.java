package steve;

import steve.data.CpuDao;
import steve.data.CpuDaoHibImplementation;
import steve.models.CPU;

public class CpuDriver {
    public static void main(String[] args) {
        CpuDao cpuDao=new CpuDaoHibImplementation();
            CPU cpu1=new CPU(1, "c1-cpu", 4, 3.0, 200.0);
            cpuDao.addNewCpu(cpu1);
            CPU cpu2=new CPU(2, "c2-cpu", 4, 3.5, 250.0);
            cpuDao.addNewCpu(cpu2);
            CPU cpu3=new CPU(3, "c3-cpu", 6, 3.8, 330.0);
            cpuDao.addNewCpu(cpu3);
    }
}
