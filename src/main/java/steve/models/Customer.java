package steve.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
@JsonIgnoreProperties("hibernateLazyInitializer")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//auto-increments
    private int customerId;
    private String userName;
    private String password;
    boolean admin;

    public Customer(){

    }

    public Customer(int customerId, String userName, String password, boolean admin) {
        this.customerId = customerId;
        this.userName = userName;
        this.password = password;
        this.admin = admin;
    }

    public Customer(int customerId, String userName, boolean admin) {
        this.customerId = customerId;
        this.userName = userName;
        this.admin = admin;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return customerId == customer.customerId && admin == customer.admin && Objects.equals(userName, customer.userName) && Objects.equals(password, customer.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerId, userName, password, admin);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId=" + customerId +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", admin=" + admin +
                '}';
    }
}
