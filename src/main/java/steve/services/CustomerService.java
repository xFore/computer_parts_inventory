package steve.services;

import steve.data.CustomerDao;
import steve.data.CustomerDaoImplementation;
import steve.models.Customer;

import java.util.List;

public class CustomerService {
    private final CustomerDao customerDao=new CustomerDaoImplementation();

    public List<Customer> getAllCustomers(){
        return customerDao.getAllCustomers();
    }

    public Customer getCustomerById(int customerId){
        return customerDao.getCustomerById(customerId);
    }

    public Customer getCustomerByUsername(String username){
        return customerDao.getCustomerByUsername(username);
    }

    public void addNewCustomer(Customer customer){
        customerDao.addNewCustomer(customer);
    }

    public void deleteCustomer(int customerId){
        customerDao.deleteCustomer(customerId);
    }

    public void updateCustomerUsername(String username, int customerId){
        customerDao.updateCustomerUsername(username, customerId);
    }
}
