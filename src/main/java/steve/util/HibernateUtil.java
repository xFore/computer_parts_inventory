package steve.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import steve.models.CPU;

import java.util.Properties;

public class HibernateUtil {
    private static SessionFactory sessionFactory;
    private static SessionFactory getSessionFactory(){
        if(sessionFactory==null){
            Configuration configuration=new Configuration();
            Properties settings=new Properties();

            settings.put(Environment.URL,"jdbc:sqlserver://steve-training-server.database.windows.net:1433;database=steve-training-db");
            settings.put(Environment.USER, "slhedstrom@steve-training-server");
            settings.put(Environment.PASS, "1!Dino5aur");
            settings.put(Environment.DRIVER, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
            settings.put(Environment.DIALECT, "org.hibernate.dialect.SQLServerDialect");
            settings.put(Environment.HBM2DDL_AUTO, "update");
            settings.put(Environment.SHOW_SQL, "true");

            configuration.setProperties(settings);

            configuration.addAnnotatedClass(CPU.class);

            sessionFactory=configuration.buildSessionFactory();
        }
        return sessionFactory;
    }

    public static Session getSession(){
        return getSessionFactory().openSession();
    }
}
