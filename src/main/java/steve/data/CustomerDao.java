package steve.data;

import steve.models.Customer;
import java.util.List;

public interface CustomerDao {
    List<Customer> getAllCustomers();
    Customer getCustomerById(int customerId);
    Customer getCustomerByUsername(String username);
    void addNewCustomer(Customer customer);
    void deleteCustomer(int customerId);
    void updateCustomerUsername(String customer, int customerId);
}
