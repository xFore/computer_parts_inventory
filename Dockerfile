FROM java:8
COPY build/libs/Computer_Part_Inventory-1.0-SNAPSHOT.jar .
EXPOSE 80
CMD java -jar Computer_Part_Inventory-1.0-SNAPSHOT.jar
